import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Data.Maybe (fromJust)
import Common

main :: IO ()
main = getContents >>= print . fromJust . intcode (12, 2) . parse
