module Common where

import Data.List.Split (splitOn)
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq

parse :: String -> Seq Int
parse = Seq.fromList . map read . splitOn "," . head . lines

intcode :: (Int, Int) -> Seq Int -> Maybe Int
intcode (x, y) field = do
    finalField <- step 0 . Seq.update 2 y . Seq.update 1 x $ field
    Seq.lookup 0 finalField

step :: Int -> Seq Int -> Maybe (Seq Int)
step position field =
    case Seq.lookup position field of
        Just 1  -> run (+)
        Just 2  -> run (*)
        Just 99 -> Just field
        _       -> Nothing
    where
        run :: (Int -> Int -> Int) -> Maybe (Seq Int)
        run op = do
            argument1 <- lookupReference (position + 1) field
            argument2 <- lookupReference (position + 2) field
            writeAddress <- Seq.lookup (position + 3) field
            let field' = Seq.update writeAddress (op argument1 argument2) field
            step (position + 4) field'

        lookupReference position field = do
            address <- Seq.lookup position field
            Seq.lookup address field
