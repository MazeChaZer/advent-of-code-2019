import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Common

main :: IO ()
main = getContents >>= mapM_ print . bruteforce output . parse

output :: Int
output = 19690720

bruteforce :: Int -> Seq Int -> [(Int, Int)]
bruteforce output field =
    [(x, y)
    | x <- [0..99]
    , y <- [0..99]
    , case intcode (x, y) field of
        Just x | x == output -> True
        _                    -> False
    ]
