import Common

main :: IO ()
main = getContents >>= print . intcode 1 . parse
