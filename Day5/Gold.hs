import Common

main :: IO ()
main = getContents >>= print . intcode 5 . parse
