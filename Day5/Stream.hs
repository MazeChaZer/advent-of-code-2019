module Stream where

import Prelude hiding (take, repeat)

infixr 5 :<
data Stream a = a :< Stream a

repeat :: a -> Stream a
repeat x = x :< repeat x

take :: Int -> Stream a -> [a]
take n (x :< xs)
    | n <= 0    = []
    | otherwise = x : take (n - 1) xs
