{-# LANGUAGE TupleSections #-}
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (fromJust)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Common

main :: IO ()
main = getContents >>= print . crossingDistance . parse

crossingDistance :: ([Movement], [Movement]) -> Int
crossingDistance (wire1, wire2) =
    fromJust . Set.lookupMin . Set.map (uncurry (+)) . Set.fromList . Map.elems $
        Map.intersectionWith (,) path1 path2
    where
        (path1, path2) = (wirePath wire1, wirePath wire2)

wirePath :: [Movement] -> Map (Int, Int) Int
wirePath wire = snd $ foldl
    (\(((x, y), originDistance), path) (direction, distance) ->
        let
            (newPosition, addedPath) =
                case direction of
                    Up ->
                        ( (x, y + distance)
                        , map (\n -> ((x, y + n), originDistance + n)) [1..distance]
                        )
                    Right' ->
                        ( (x + distance, y)
                        , map (\n -> ((x + n, y), originDistance + n)) [1..distance]
                        )
                    Down ->
                        ( (x, y - distance)
                        , map (\n -> ((x, y - n), originDistance + n)) [1..distance]
                        )
                    Left' ->
                        ( (x - distance, y)
                        , map (\n -> ((x - n, y), originDistance + n)) [1..distance]
                        )
        in
        ( (newPosition, originDistance + distance)
        , Map.union path (Map.fromList addedPath)
        )
    )
    (((0, 0), 0), Map.empty)
    wire

manhattenDistance :: (Int, Int) -> (Int, Int) -> Int
manhattenDistance (x1, y1) (x2, y2) = abs (x2 - x1) + abs (y2 - y1)
