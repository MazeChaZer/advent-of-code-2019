module Common where

import Data.List.Split (splitOn)

data Direction = Up | Right' | Down | Left'

type Movement = (Direction, Int)

parse :: String -> ([Movement], [Movement])
parse input =
    (parseWire line1, parseWire line2)
    where
        [line1, line2] = lines input
        parseWire = map parseMovement . splitOn ","

parseMovement :: String -> Movement
parseMovement (x:xs) =
    ( case x of
        'U' -> Up
        'R' -> Right'
        'D' -> Down
        'L' -> Left'
    , read xs
    )
