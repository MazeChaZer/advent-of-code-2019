{-# LANGUAGE TupleSections #-}
import Data.List.Split (splitOn)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (fromJust)
import Common

main :: IO ()
main = getContents >>= print . crossingDistance . parse

crossingDistance :: ([Movement], [Movement]) -> Int
crossingDistance (wire1, wire2) =
    fromJust . Set.lookupMin . Set.map (manhattenDistance (0, 0)) $
        Set.intersection path1 path2
    where
        (path1, path2) = (wirePath wire1, wirePath wire2)

wirePath :: [Movement] -> Set (Int, Int)
wirePath wire = snd $ foldl
    (\((x, y), path) (direction, distance) ->
        let
            (newPosition, addedPath) =
                case direction of
                    Up ->
                        ( (x, y + distance)
                        , map (x,) [y + 1..y + distance]
                        )
                    Right' ->
                        ( (x + distance, y)
                        , map (,y) [x + 1..x + distance]
                        )
                    Down ->
                        ( (x, y - distance)
                        , map (x,) [y - 1, y - 2..y - distance]
                        )
                    Left' ->
                        ( (x - distance, y)
                        , map (,y) [x - 1, x - 2..x - distance]
                        )
        in (newPosition, Set.union path (Set.fromList addedPath))
    )
    ((0, 0), Set.empty)
    wire

manhattenDistance :: (Int, Int) -> (Int, Int) -> Int
manhattenDistance (x1, y1) (x2, y2) = abs (x2 - x1) + abs (y2 - y1)
