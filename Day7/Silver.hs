import Data.List (permutations)
import Data.Sequence (Seq)
import Common

main :: IO ()
main = getContents >>= print . maximumPower . parse

maximumPower :: Seq Int -> Int
maximumPower field = maximum . map (runCircuit field) . permutations $ [0..4]

runCircuit :: Seq Int -> [Int] -> Int
runCircuit field = foldl f 0
    where
        f power phase = output
            where Just (_, [output]) = intcode field [phase, power]
