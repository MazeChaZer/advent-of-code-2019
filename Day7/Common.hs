module Common where

import Data.List.Split (splitOn)
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Stream (Stream(..))
import qualified Stream

data Instruction
    = Plus
    | Multiply
    | Input
    | Output
    | JumpIfTrue
    | JumpIfFalse
    | LessThan
    | Equals
    | Exit
    deriving (Show, Eq)

data Mode
    = Position
    | Immediate
    deriving (Show, Eq)

parse :: String -> Seq Int
parse = Seq.fromList . map read . splitOn "," . head . lines

intcode :: Seq Int -> [Int] -> Maybe (Seq Int, [Int])
intcode field inputs = step inputs 0 field

argumentModes :: Int -> Maybe (Stream Mode)
argumentModes opcode = loop $ opcode `div` 100
    where
        loop n =
            (:<)
                <$> (case n `mod` 10 of
                        0 -> Just Position
                        1 -> Just Immediate
                        _ -> Nothing
                    )
                <*> (let next = n `div` 10 in
                        if next == 0 then
                            Just $ Stream.repeat Position
                        else
                            loop next
                    )

step :: [Int] -> Int -> Seq Int -> Maybe (Seq Int, [Int])
step inputs position field = do
    opcode <- Seq.lookup position field
    instruction <-
        case opcode `mod` 100 of
            1  -> Just Plus
            2  -> Just Multiply
            3  -> Just Input
            4  -> Just Output
            5  -> Just JumpIfTrue
            6  -> Just JumpIfFalse
            7  -> Just LessThan
            8  -> Just Equals
            99 -> Just Exit
            _  -> Nothing
    modes <- argumentModes opcode
    case instruction of
        Plus        -> runBinaryOperator modes (+)
        Multiply    -> runBinaryOperator modes (*)
        Input       -> do
            (input, inputs') <-
                case inputs of
                    x:xs -> Just (x, xs)
                    []   -> Nothing
            field' <- intcodeWrite (position + 1) input field
            step inputs' (position + 2) field'
        Output      -> do
            let mode1 :< _ = modes
            value <- intcodeRead field mode1 (position + 1)
            (field', outputs') <- step inputs (position + 2) field
            return (field', value : outputs')
        JumpIfTrue  -> jump modes (/= 0)
        JumpIfFalse -> jump modes (== 0)
        LessThan    -> runBinaryOperator modes (\x y -> fromEnum $ x < y)
        Equals      -> runBinaryOperator modes (\x y -> fromEnum $ x == y)
        Exit        -> Just (field, [])
    where
        runBinaryOperator :: Stream Mode
                          -> (Int -> Int -> Int)
                          -> Maybe (Seq Int, [Int])
        runBinaryOperator modes op = do
            let mode1 :< mode2 :< _ = modes
            argument1 <- intcodeRead field mode1 (position + 1)
            argument2 <- intcodeRead field mode2 (position + 2)
            field' <-
                intcodeWrite (position + 3) (op argument1 argument2) field
            step inputs (position + 4) field'

        jump modes p = do
            let mode1 :< mode2 :< _ = modes
            condition <- intcodeRead field mode1 (position + 1)
            if p condition then do
                address <- intcodeRead field mode2 (position + 2)
                step inputs address field
            else
                step inputs (position + 3) field
                

intcodeRead :: Seq Int -> Mode -> Int -> Maybe Int
intcodeRead field mode position = do
    value <- Seq.lookup position field
    case mode of
        Position  -> Seq.lookup value field
        Immediate -> Just value

intcodeWrite :: Int -> Int -> Seq Int -> Maybe (Seq Int)
intcodeWrite position value field = do
    address <- Seq.lookup position field
    return $ Seq.update address value field
