import Common

main :: IO ()
main = handleMain $ fuelFuel . oneStep

fuelFuel :: Int -> Int
fuelFuel = sum . takeWhile (> 0) . iterate oneStep
