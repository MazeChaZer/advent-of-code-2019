module Common where

handleMain :: (Int -> Int) -> IO ()
handleMain moduleRequirement =
    getContents >>= print . (fuelRequirement moduleRequirement)

fuelRequirement :: (Int -> Int) -> String -> Int
fuelRequirement moduleRequirement =
    sum . map (moduleRequirement . read) . lines

oneStep :: Int -> Int
oneStep mass = mass `div` 3 - 2 
