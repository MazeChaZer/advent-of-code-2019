module Common where

input :: String
input = "109165-576723"

monotone :: [Int] -> Bool
monotone = all (uncurry (<=)) . pairs

digits :: Int -> [Int]
digits = map (read . pure) . show

infixl 3 &&&
(&&&) :: (a -> Bool) -> (a -> Bool) -> a -> Bool
f &&& g = \x -> f x && g x

pairs :: [a] -> [(a, a)]
pairs = zip <*> tail
