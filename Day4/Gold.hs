import Data.List.Split (splitOn)
import Data.List (group)
import Common

main :: IO ()
main = print . searchPasswords . parse $ input

parse :: String -> (Int, Int)
parse input = (min, max)
    where [min, max] = map read . splitOn "-" $ input

searchPasswords :: (Int, Int) -> Int
searchPasswords (min, max) = length . filter possiblePassword $ [min..max]

possiblePassword :: Int -> Bool
possiblePassword = (containsTwoGroup &&& monotone) . digits

containsTwoGroup :: [Int] -> Bool
containsTwoGroup = (2 `elem`) . map length . group
