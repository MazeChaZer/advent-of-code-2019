import Data.List.Split (splitOn)
import Common

main :: IO ()
main = print . searchPasswords . parse $ input

parse :: String -> (Int, Int)
parse input = (min, max)
    where [min, max] = map read . splitOn "-" $ input

searchPasswords :: (Int, Int) -> Int
searchPasswords (min, max) = length . filter possiblePassword $ [min..max]

possiblePassword :: Int -> Bool
possiblePassword = (containsEqualNeighbors &&& monotone) . digits

containsEqualNeighbors :: [Int] -> Bool
containsEqualNeighbors = any (uncurry (==)) . pairs
