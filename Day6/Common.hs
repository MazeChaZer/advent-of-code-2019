module Common where

import Data.List.Split (splitOn)

parse :: String -> [(String, String)]
parse = map (pair . splitOn ")") . lines
    where pair [x, y] = (x, y)
