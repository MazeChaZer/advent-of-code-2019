import Common

main :: IO ()
main = getContents >>= print . measureDistance . parse

measureDistance :: [(String, String)] -> Int
measureDistance orbitMap = distance
    where
        [distance] = loop "YOU" $ parent "YOU"
        
        loop origin object
            | (object, "SAN") `elem` orbitMap = [0]
            | otherwise                       =
                  map (+ 1)
                . concatMap (loop object)
                . breadth origin 
                $ object

        parent object = p
            where [(p, _)] = filter ((== object) . snd) $ orbitMap

        breadth origin object =
            filter (/= origin) $
                   (map fst . filter ((== object) . snd) $ orbitMap)
                ++ (map snd . filter ((== object) . fst) $ orbitMap)
