import Common

main :: IO ()
main = getContents >>= print . countOrbits . parse

countOrbits :: [(String, String)] -> Int
countOrbits orbitMap = loop 0 "COM"
    where
        loop predecessors object =
            predecessors
            + ( sum
              . map (loop (predecessors + 1) . snd)
              . filter ((== object) . fst)
              $ orbitMap
              )
